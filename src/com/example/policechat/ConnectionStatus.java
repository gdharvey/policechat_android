package com.example.policechat;

import java.io.Serializable;

public enum ConnectionStatus implements Serializable {
	Disconnected,
	Connected,
	Authenticated
}
