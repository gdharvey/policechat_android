package com.example.policechat;

import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class PoliceChat extends Activity implements ChatClientInterface{

	private volatile ClientEngine clientengine;
	//private PoliceOfficer anID;
	private TextView theOutput;
	private EditText theInput;
	private TextView theUsername;
	private TextView thePassword;
	
	/** Used for identifying info in logs.*/
	private static final String TAG = "PoliceChat";	
	
	public String intToIp(int integer) {
        return (integer & 0xFF) + "." + ((integer >> 8) & 0xFF) + "."
                + ((integer >> 16) & 0xFF) + "." + ((integer >> 24) & 0xFF);
    }	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		
		DhcpInfo dhcpInfo = ((WifiManager)getSystemService(Context.WIFI_SERVICE)).getDhcpInfo();
		String ipaddress = intToIp(dhcpInfo.ipAddress);
		Log.d(TAG, "IP address: " + ipaddress);
		
		setContentView(R.layout.activity_police_chat);
		theUsername = (TextView)this.findViewById(R.id.tvUsername);
		thePassword = (TextView)this.findViewById(R.id.tvPassword);
		theOutput = (TextView)this.findViewById(R.id.tvOutput);
		theInput = (EditText)this.findViewById(R.id.tvInput);
		theInput.setOnEditorActionListener(new OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        boolean handled = false;
		        if (actionId == EditorInfo.IME_ACTION_SEND) {
		        	clientengine.SendData(theInput.getText().toString());
		        	theInput.setText("");
		            handled = true;
		        }
		        return handled;
		    }
		});
		
//		theInput.setKeyListener(new KeyListener() {
//			public boolean onKey(View v, int keyCode, KeyEvent event) {
//		        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_DPAD_CENTER)) {
//		        	SendToEngine ((String) theInput.getText(), clientengine);
//		        	theInput.setText("");
//		            return true;
//		        }
//		        return false;
//	        }
//
//			@Override
//			public void clearMetaKeyState(View arg0, Editable arg1, int arg2) {
//				// TODO Auto-generated method stub
//				
//			}
//
//			@Override
//			public int getInputType() {
//				// TODO Auto-generated method stub
//				return 0;
//			}
//
//			@Override
//			public boolean onKeyDown(View arg0, Editable arg1, int arg2,
//					KeyEvent arg3) {
//				// TODO Auto-generated method stub
//				return false;
//			}
//
//			@Override
//			public boolean onKeyOther(View arg0, Editable arg1, KeyEvent arg2) {
//				// TODO Auto-generated method stub
//				return false;
//			}
//
//			@Override
//			public boolean onKeyUp(View arg0, Editable arg1, int arg2,
//					KeyEvent arg3) {
//				// TODO Auto-generated method stub
//				return false;
//			}
//		});
		
	}
	
	public void doLogin (View v)
	{
		String aUsername = theUsername.getText().toString();
		String aPassword = thePassword.getText().toString();
		if ((clientengine.IsRunning()) 
				&& (aUsername != null) && (!aUsername.isEmpty()) 
				&& (aPassword != null) && (!aPassword.isEmpty()))
		{
			ChatAuthenticator anAuthenticator = new ChatAuthenticator();
			anAuthenticator.Username = aUsername;
			anAuthenticator.Password = aPassword;
			clientengine.SendData(anAuthenticator);
		}
	}
	
	
	@Override
	protected void onResume()
	{
		super.onResume();
		clientengine = new ClientEngine(this);
//		if (anID != null)
//			clientengine.setID(anID);
		Thread theThread = new Thread (clientengine);
		theThread.start();
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		if (clientengine != null)
		{
			clientengine.ShutDownEngine();
			clientengine = null;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_police_chat, menu);
		return true;
	}

//	@Override
//	public void ReceiveFromEngine(String aValue) {
//		theOutput.append(aValue + "\n");
//	}
//
//	@Override
//	public void SendToEngine(String aValue, ClientEngine anEngine) {
//		if (anEngine.IsRunning())
//			anEngine.SendData(aValue);
//		else
//			CreateDialog (R.string.engine_failed);
//	}
	
	@Override
	public void ReceiveFromEngine(Object aValue) {
		// TODO Auto-generated method stub
		
	}
	
	
	/**
	 * Creates a message box with string id specified.
	 * @param aStringID The resource ID of the string.
	 */
	private void CreateDialog (int aStringID)
	{
		//Show a message indicating save done
		AlertDialog.Builder aDBuilder = new AlertDialog.Builder(this);
		aDBuilder.setMessage(aStringID);
		aDBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// We need to include this element to get the positive button, but
				// don't actually need to do anything here - therefore empty definition.
			}
		});
		//Show the message box.
		aDBuilder.create().show();
	}

}
