package com.example.policechat;

public final class ChatClientConfig {
	public static class ClientEngine {
		//public static final String ChatDestinationIP = "127.0.0.1";
		public static final String ChatDestinationIP = "10.70.225.146";
		//public static final String ChatDestinationIP = "192.168.37.109";
		public static final int DestPortNumber = 5000;
		public static final int threadSleepInterval = 200;
	}
}
