package com.example.policechat;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;


//import android.util.Log;

/**
 * This class handles the communication related portions for connections between
 * the app and the chat server. It communicates to a handed in ChatClientInputOutput
 * object (an instance of the ChatClientInputOutput interface).
 * @author Geoff Harvey
 *
 */
public class ClientEngine implements Runnable {
	private ConnectionStatus aStatus;
	private Socket theSocket;
	private ObjectOutputStream anOutStream;
	private ObjectInputStream anInStream;
	private BufferedInputStream inBuff;
	private volatile Boolean ShutDownFlag = false;
	private volatile ChatClientInterface anInterface;
	
	/** Used for identifying info in logs.*/
	private static final String TAG = "ClientEngine";
	
	public void ShutDownEngine()
	{
		ShutDownFlag = true;
	}
	
	public Boolean IsRunning()
	{
		return !ShutDownFlag;
	}
	
	public ClientEngine(ChatClientInterface theFace)
	{
		anInterface = theFace;
		aStatus = ConnectionStatus.Disconnected;
	}
	
	public void run()
	{
		try {
//			Log.d(TAG, "Starting ClientEngine");
			theSocket = new Socket 
					(InetAddress.getByName(ChatClientConfig.ClientEngine.ChatDestinationIP), 
							ChatClientConfig.ClientEngine.DestPortNumber);
			anOutStream = new ObjectOutputStream (theSocket.getOutputStream());
			anOutStream.flush();
			anInStream = new ObjectInputStream (theSocket.getInputStream());
			//inBuff = new BufferedInputStream(anInStream);
			inBuff = new BufferedInputStream(theSocket.getInputStream());
//			Log.d(TAG, "ClientEngine successfully started");
		} catch (UnknownHostException e) {
//			Log.d(TAG, "UnknownHostException: " + e.getMessage());
			ShutDownEngine ();
			
		} catch (IOException e) {
//			Log.d(TAG, "IOException: " + e.getMessage());
			ShutDownEngine();
		}
		
		Object theInput;
		while (!ShutDownFlag)
		{
			try
			{
				//This is a little trick I picked up. Basically, it's really
				//hard to know if an object is waiting using just an ObjectInputStream.
				//But if you simultaneously open a BufferedInputStream on that stream
				//as well, you can use the BufferedInputStream available method, which
				//reliably lets you know the number of byes waiting. Unfortunately,
				//while ObjectInputStream has an available() method, it doesn't
				//provide this information reliably.
				if (inBuff.available() > 0)
				{
					//This next statement might trigger a ClassNotFoundException
					//if nothing there.
					theInput = anInStream.readObject();
					if (theInput.getClass() == ConnectionStatus.class)
					{
						aStatus = (ConnectionStatus)theInput;
					}
					anInterface.ReceiveFromEngine (theInput);
//					if (theInput.getClass() == String.class)
//					{
//						anInterface.ReceiveFromEngine ((String)theInput);
//					}
//					else if (theInput.getClass() == ConnectionStatus.class)
						
				}
				Thread.sleep(ChatClientConfig.ClientEngine.threadSleepInterval);
			}
			catch (InterruptedException iex)
			{
//				Log.d(TAG, iex.getMessage());
			}
			catch (IOException iox)
			{
//				Log.d(TAG, iox.getMessage());
			}
			catch (ClassNotFoundException cnfex)
			{
//				Log.d(TAG, cnfex.getMessage());
			}
		}
		try {
			anInStream.close();
			inBuff.close();
			theSocket.close();
			anOutStream.close();
		}
		catch (Exception ex)
		{
			//Suppress.
		}

	}
	
	public void SendData (Object someInput)
	{
		try {
			
			if ((someInput.getClass() == String.class) && (aStatus == ConnectionStatus.Authenticated))
			{
				//String theSentMessage = anID.name + ">>" + (String)someInput;
				anOutStream.writeObject(someInput);
				anOutStream.flush();
			}
			else if (someInput.getClass() == ChatAuthenticator.class)
			{
				anOutStream.writeObject(someInput);
				anOutStream.flush();				
			}
		}
		catch (IOException ioex)
		{
//			Log.d (TAG, "IOException in SendData: " + ioex.getMessage());
		}
	}
	
}